# Question 1

Pour se connecter il faut que les deux utilisateurs

- utilisent leurs mot de passe
- branchent leurs clé USB

Pour ce faire, les deux utilisateurs ont un élément permettant de décrypter le fichier de données

Pour l'instant, les deux utilisateurs ont une moitié de clé, qu'on concatène pour obtenir la clé finale
Cette solution n'est pas viable

# Question 3

Pour répondre à ce problème, on créé des réprésentants (avec des lmots de passe titi et toto, peu importe)
Les responsables peuvent activer ou désactiver leurs réprésentants
Cela va modifier un booléen dans le fichier sur le serveur
Si un représentant se connecte avec la même clé que son responsable, et un mot de passe, il pourra décrypter et accéder aux données
Les deux ont les mêmes droits

# Question 5

Pour exclure une personne, il faut que les 3 autres personnes se connectent en donnant leurs mot de passe
Ensuite il faudra aussi gérer les clés usb, pour ce faire on demande une authentification supplémentaire (on n'est jamais trop sûr), qui demandera les clés usb

Une fois que tout le monde est authentifié, on applique un processus simple :

- on décrypte les données
- on créé une nouvelle clé et on redonne aux 3 personnes leurs éléments permettant
- on crypte les données avec la nouvelle clé
