from cryptography.fernet import Fernet
import csv

USB_PATH = "src/usb_key/"
TEMP_PATH = "src/temp/"
FILENAME = "src/data.csv"

KEY = ""


def authentication():
    isOk = True

    # User 1
    pwd = str(input("Password user 1 ?\t"))
    file_pwd = open(TEMP_PATH + "user_1")

    try:
        file_prompt_key = open(USB_PATH + "key_1")

        key_part1 = file_prompt_key.read()

        if (pwd == file_pwd.read()):
            print("User 1 - OK")
        else:
            file_pwd = open(TEMP_PATH + "repr_1")
            var_pwd, access = file_pwd.readlines()
            var_pwd = var_pwd.replace("\n", "")

            if (pwd == var_pwd and access == "True"):
                print("Repr 1 - OK")
            elif (pwd == var_pwd):
                print("Repr 1 - Non autorisé par le responsable")
                isOk = False
            else:
                print("User 1 - NOK")
                isOk = False

        file_prompt_key.close()
        
        
    except IOError:
        print("Aucune clé USB rentré")
    finally:
        pass        

    # User 2
    pwd = str(input("Password user 2 ?\t"))
    file_pwd = open(TEMP_PATH + "user_2")

    try:
        file_prompt_key = open(USB_PATH + "key_2")
        
        key_part2 = file_prompt_key.read()
        if (pwd == file_pwd.read()):
            print("User 2 - OK")
        else:
            file_pwd = open(TEMP_PATH + "repr_2")
            var_pwd, access = file_pwd.readlines()
            var_pwd = var_pwd.replace("\n", "")
            
            if (pwd == var_pwd and access == "True"):
                print("Repr 2 - OK")
            elif (pwd == var_pwd):
                print("Repr 2 - Non autorisé par le responsable")
                isOk = False
            else:
                print("User 2 - NOK")
                isOk = False
        
        file_prompt_key.close()
        
    except IOError:
        print("Aucune clé USB rentré")
    finally:
        pass

    if (isOk):
        print("-> Authentification réussie")
        return key_part1 + key_part2
    else:
        return False

def init():
    # Person 1
    f = open(TEMP_PATH + "user_1", "w")
    f.write(str(input("Password ?\t")))
    f.close()

    # Person 2
    f = open(TEMP_PATH + "user_2", "w")
    f.write(str(input("Password ?\t")))
    f.close()

def createKey():
    key = str(Fernet.generate_key().decode("utf-8"))
    
    firstpart, secondpart = key[:int(len(key)/2)], key[int(len(key)/2):]

    f = open(USB_PATH + "key_1", "w")
    f.write(firstpart)
    f.close()

    
    f = open(USB_PATH + "key_2", "w")
    f.write(secondpart)
    f.close()

  
    return key


def encrypt(key):
    f = Fernet(key)
    with open(FILENAME, "rb") as file:
        # read all file data
        file_data = file.read()
        file.close()

    encrypted_data = f.encrypt(file_data)

    with open(FILENAME, "wb") as file:
        file.write(encrypted_data)
        file.close()

def decrypt(key):
    f = Fernet(key)
    with open(FILENAME, "rb") as file:
        encrypted_data = file.read()
        file.close()
    decrypted_data = f.decrypt(encrypted_data)
    with open(FILENAME, "wb") as file:
        file.write(decrypted_data)
        file.close()

def addPair(key):
    try:
        decrypt(key)
    except Exception:
        print("Already decrypted")

    name = str(input("Name :\t"))
    card = str(input("Card :\t"))

    with open(FILENAME, "a") as file:
        file.write(name + ";" + card + "\n")
        file.close()

    # encrypt(key)

def deletePair(key):
    try:
        decrypt(key)
    except Exception:
        print("Already decrypted")

    name = str(input("Name :\t"))
    card = str(input("Card :\t"))
    line = name + ";" + card + "\n"

    with open(FILENAME, "r") as file:
        lines = file.readlines()
        file.close()

    try:
        lines.remove(line)
    except Exception:
        print("Pair doesn't exist")
    
    with open(FILENAME, "w") as file:
        for l in lines:
            file.write(l)
        file.close()
    
def searchPair(key):
    try:
        decrypt(key)
    except Exception:
        print("Already decrypted")

    prompt_name = str(input("Name :\t"))

    with open(FILENAME, "r") as file:
        lines = file.readlines()
        file.close()

    for l in lines:
        name, card = l.replace('\n', '').split(";")
        if (prompt_name == name):
            print(card)

def setRepresentant():

    num = int(input("Un réprésentant pour quel utilisateur ? "))
    # User 
    pwd = str(input("Password user ? \t"))
    file_pwd = open(TEMP_PATH + "user_" + str(num))

    if (pwd == file_pwd.read()):
        access = input("Lui donner l'accès ? True / False\n")
        
        f = open(TEMP_PATH + "repr_" + str(num), "r")
        lines = f.readlines()
        lines[1] = str(access)
    
        f = open(TEMP_PATH + "repr_" + str(num), "w")
        f.writelines(lines)
        f.close()

        print("-> Représentant modifié")
    else:
        print("-> Mot de passe incorrect")


def repudiation():
    print("Qui souhaitez répudier ?")
    print("1 - User 1")
    print("2 - User 2")
    print("3 - Repr 1")
    print("4 - Repr 2")
    print("5 - Personne")

    num = int(input())

    file_pwd_user1 = open(TEMP_PATH + "user_1", "r")
    file_pwd_user2 = open(TEMP_PATH + "user_2", "r")
    file_pwd_repr1 = open(TEMP_PATH + "repr_1", "r")
    file_pwd_repr2 = open(TEMP_PATH + "repr_2", "r")

    mdp_repr1, _ = file_pwd_repr1.readlines()
    mdp_repr1 = mdp_repr1.replace("\n", "")

    mdp_repr2, _ = file_pwd_repr2.readlines()
    mdp_repr2 = mdp_repr2.replace("\n", "")

    if (num == 5):
        return
    elif (num == 1):
        
        if (file_pwd_user2.read() == str(input("Mot de passe utilisateur 2 ? ")) and mdp_repr1 == str(input("Mot de passe représentant 1 ? ")) and mdp_repr2 == str(input("Mot de passe représentant 2 ? "))):
            kick(1)
        else:
            print("Problème de mot de passe")
    elif (num == 2):
        if (file_pwd_user1.read() == str(input("Mot de passe utilisateur 1 ? ")) and mdp_repr1 == str(input("Mot de passe représentant 1 ? ")) and mdp_repr2 == str(input("Mot de passe représentant 2 ? "))):
            kick(2)
        else:
            print("Problème de mot de passe")
    elif (num == 3):
        if (file_pwd_user2.read() == str(input("Mot de passe utilisateur 2 ? ")) and file_pwd_user1.read() == str(input("Mot de passe user 1 ? ")) and mdp_repr2 == str(input("Mot de passe représentant 2 ? "))):
            kick(3)
        else:
            print("Problème de mot de passe")
    elif (num == 4):
        if (file_pwd_user2.read() == str(input("Mot de passe utilisateur 2 ? ")) and mdp_repr1 == str(input("Mot de passe représentant 1 ? ")) and file_pwd_user1.read() == str(input("Mot de passe user 1 ? "))):
            kick(4)
        else:
            print("Problème de mot de passe")
            

def kick(num):
    print("Authentification nécessaire")
    KEY = authentication()
    
    try:
        decrypt(KEY)
        print("Decryptage réussi")
    except:
        print("Déjà décrypté")

    KEY = createKey()

    encrypt(KEY)


# createKey()


# Menu
alive = True
connected = False

while (alive):
    if (not connected):
        print("Que faire ?")
        print("1 - Se connecter")
        print("2 - Initialisation")
        print("0 - Quitter")

        choice = int(input())

        if (choice == 1):
            KEY = authentication()
        elif (choice == 2):
            createKey()
            init()
        elif (choice == 0):
            alive = False
            break

        if (KEY != False):
            connected = True


    else:
        print("Que faire ?")
        print("3 - Chercher dans la base")
        print("4 - Ajouter une paire")
        print("5 - Supprimer une paire")
        print("6 - Choisir un réprésentant")
        print("7 - Répudier quelqu'un")
        print("8 - Encryptage des données")
        print("9 - Décryptage des données")
        print("0 - Quitter")

        choice = int(input())
    
        if (choice == 3):
            searchPair(KEY)
        elif (choice == 4):
            addPair(KEY)
        elif (choice == 5):
            deletePair(KEY)
        elif (choice == 6):
            setRepresentant()
        elif (choice == 7):
            repudiation()
        elif (choice == 8):
            encrypt(KEY)
        elif (choice == 9):
            decrypt(KEY)
        elif (choice == 0):
            alive = False
            break
    
                    
